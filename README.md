# lorem

__lorem__ is a placeholder repository like the placeholder text [Lorem ipsum](https://en.wikipedia.org/wiki/Lorem_ipsum).
Use it for training or develoment purposes.

* markdown
* [Git Large File Storage](https://git-lfs.github.com/) with [.gitattributes](/.gitattributes) with examples in  [/img](/img)
* [PROSErial](https://github.com/klml/proserial)

Content:

* plain website [europan.md](europan.md)
* blog: in flat style in [/2016](/2016) and subdirectories for months in [/2017](/2017).
* issue tracking: [/issues](/issues)